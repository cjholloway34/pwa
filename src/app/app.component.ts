import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-root',
  template: `
    <div class="pwa-note">
      <app-header></app-header>
      <div class="main">
        <router-outlet></router-outlet>
      </div>
      <app-footer></app-footer>
    </div>
  `,
  styleUrls: ['./app.component.styl']
})
export class AppComponent {
  title = 'ombm-pwa';

  constructor(db: AngularFirestore) {}
}
