export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyC03ZtGtP_l7hEFBgB2MOHsfS_FEA0_K6k',
    authDomain: 'ombm-ng-pwa.firebaseapp.com',
    databaseURL: 'https://ombm-ng-pwa.firebaseio.com',
    projectId: 'ombm-ng-pwa',
    storageBucket: 'ombm-ng-pwa.appspot.com',
    messagingSenderId: '600059176291',
    appId: '1:600059176291:web:b3831acca100a4de'
  }
};
