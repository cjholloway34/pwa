# Tradeoffs when Using Pug with Angular (with CLI)

Pug CLI allows you to compile pug with command like `pug [dir,file]`` and watch the changes.

<pre name="2591" id="2591" class="graf graf--pre graf-after--p">npm install pug-cli -D</pre>

After installation, create two scripts in `package.json``:

<pre name="6c3e" id="6c3e" class="graf graf--pre graf-after--p">"pug-it": "pug src",  
"pug-it:w": "pug src -P -w"</pre>

The first command `pug-it` will compile all of the .pug files in the `src` directory to .html file in same directory.

The second command does exactly the same but adds file watching. We use this command during development to watch the file changes and recompile automatically.

Here are more npm scripts.

<pre name="3372" id="3372" class="graf graf--pre graf-after--p">"ng": "ng",  
"start": "run-p pug-it:w server",  
"server": "ng serve --port 4210",  
"prebuild": "yarn run pug-it",  
"build": "ng build",</pre>

Please notes that:-

1.  Running `npm start`, spins up both the dev server, and the pug watch tasks concurrently.
2.  Before building, pug compiles templates during the `prebuild` phase.

#### Side notes on concurrent tasks

[concurrently](https://www.npmjs.com/package/concurrently) and [npm-run-all](https://www.npmjs.com/package/npm-run-all) are helpful to run multiple tasks concurrently across platforms (mac, windows, etc). We use `npm-run-all`. The `run-p` command is provided by the package. You can install it:

<pre name="e5b2" id="e5b2" class="graf graf--pre graf-after--p">npm install npm-run-all --save-dev</pre>

Linux or Mac users can disregard this step and replace `start` command with`npm run pug-it:w & npm run server` will do.

### Git Ignore HTML files

If you are using pug, you might not want to check in the generated HTML files. Exclude them in your git. Add this line to your `.gitignore` file.

<pre name="da7f" id="da7f" class="graf graf--pre graf-after--p">#pug  
/src/**/*.html</pre>

Note- component typescript fils still refer to `.html`,and **NOT** `.pug` files.

<pre name="aca2" id="aca2" class="graf graf--pre graf-after--p">@Component({  
   selector: 'my-root',  
   templateUrl: './app.component.html',  
   styleUrls: ['./app.component.scss']  
})  
export class AppComponent {}</pre>

### Trade-offs

There are a few tradeoffs using this approach.

#### **1\. No auto watching for new files**

When adding any new pug file after `npm start`, the new file is not watched. You need to restart dev server (stop and rerun `npm start`).

#### 2\. Fail silently when hitting pug syntax error

Since we start pug watch and dev-server concurrently, when there’s pug syntax error happens during development, you will see errors in terminal, but not on screen or browser console. Please beware of this, sometimes you got syntax error without knowing it, and spend hours to debug other area (you know what I mean).

#### 3\. Need to manually create pug file if you use `ng generate`

By default, angular-cli `ng generate` will generate HTML file for component. You need to rename or delete/create the HTML to pug file.

### Some captcha when using pug with Angular

When using Angular with Pug, Angular's template syntax requires the developer to surround all attributes with single quotes.

This will generate the following HTML.

<pre name="f0dc" id="f0dc" class="graf graf--pre graf-after--p">my-top-bar(  
  '[(ngModel)]'="myModel"  
  '[propA]'="myPropA"  
  '(actionA)'="handle($event)"  
)</pre>

If single quote on the attributes are lacking single quotes, No error will be notify in terminal.
All fields will be omitted.

The HTML generated will be

<pre name="6351" id="6351" class="graf graf--pre graf-after--p"><my-top-bar>  
</my-top-bar></pre>

**POTENTIAL DEBUGGING GOTCHA!!!**
